
import { Routes, Route,  BrowserRouter } from 'react-router-dom';
import './App.css';
import NavbarPages from './components/Navbar';
import About from './Page/About';
import UpcomingMatches from './Page/UpcomingMatches';
import Home from './Page/Home';

function App() {
  return (
    <>
      <BrowserRouter>
        <NavbarPages />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/upcomingMatches" element={<UpcomingMatches />} />
          <Route path="/about" element={<About />} />

        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
