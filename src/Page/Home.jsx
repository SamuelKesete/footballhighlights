
import { useEffect } from 'react';
import { useState } from 'react';
import axios from 'axios';
import { REACT_APP_API_KEY } from '../key/ApiKey';
import '../App.css'
import Post from '../components/Post';
import Pagination from '../components/Pagination';

function Home() {

    const [post, setPost] = useState([]);
    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [postPerpage] = useState(9);



    useEffect(() => {
        const fetchPost = async () => {
            setLoading(true);
            const res = await axios.get(`https://www.scorebat.com/video-api/v3/feed/?token=${REACT_APP_API_KEY}`);
            setPost(res.data.response);
            setLoading(false);
        }
        fetchPost();

    }, []);


    const indexOfLastPost = currentPage * postPerpage;
    const indexOfFirstPost = indexOfLastPost - postPerpage;
    const currentPosts = post.slice(indexOfFirstPost, indexOfLastPost);


    // change page
    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    return (

        <div className='container mt-5'>

            <Post post={currentPosts} loading={loading} />
            <Pagination postPerpage={postPerpage} totalPost={post.length} paginate={paginate} 
            />
        </div>



    )
}

export default Home