import React from 'react';
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Form from 'react-bootstrap/Form';
import '../App.css';

function NavbarPages() {
    return (
        <>

            <Navbar collapseOnSelect expand="lg" bg="light" variant="light" className="Navbar p-3">
                <Container>
                    <Navbar.Brand
                        className='text-decoration-none text-dark
                      p-2 m-2 font-italic
                      
                      d-flex align-items-start'

                    >
                        Football Highlights
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <>

                            <Nav className="nav me-auto">
                                <Nav.Link className='navLink'>
                                    <Link
                                        className="text-decoration-none text-primary 
                                    text-uppercase p-2 m-2 fw-bolder " to="/"

                                    >
                                        Home
                                    </Link>
                                </Nav.Link>

                                <Nav.Link>
                                    <Link className="text-decoration-none text-primary 
                                    text-uppercase p-2 m-2 fw-bolder "
                                        to="/upcomingMatches">
                                        Upcoming matches
                                    </Link>
                                </Nav.Link>
                                <Nav.Link>

                                    <Link
                                        className="text-decoration-none text-primary 
                                        text-uppercase p-2 m-2 fw-bolder "
                                        to="/about">
                                        About
                                    </Link>
                                </Nav.Link>
                            </Nav>
                            <Nav className="gap-2">
                                <Form.Control type="text" placeholder="Search" />

                            </Nav>
                        </>

                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}

export default NavbarPages