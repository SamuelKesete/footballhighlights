import React from 'react'
import { Link } from 'react-router-dom';
import '../App.css';



const Pagination = ({ postPerpage, totalPost, paginate }) => {
    // 1. empty pageNumbers variable
    const pageNumbers = [];

    // index start at one and if the index is <= totalPost / by postPerpage 
    // then wrap tha up with (Math.ceil)
    for (let index = 1; index <= Math.ceil(totalPost / postPerpage); index++) {

        // take the pageNumber and push the (index) into it 
        pageNumbers.push(index);

    }

    return (
        <nav className='paginationnav text-end'>
            <ul className='pagination text-end'>
                { 
                pageNumbers.map(number=>(
                    <li key={number} className='page-item'>
                        <Link onClick={()=> paginate(number)}  className='page-link'>
                            {number}
                        </Link>

                    </li>
                ) )
                
                }
            </ul>
        </nav>
    )
}

export default Pagination