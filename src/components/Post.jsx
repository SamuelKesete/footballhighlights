import { Card, Col, Row } from 'react-bootstrap';
import '../App.css'



const Post = ({ post, loading }) => {
    if (loading) {
        return <p> Loading ......</p>

    }
    return (




        <Row className="row">
            {
                post.map((item, k) => (
                    <Col key={k} md={6} lg={4} className='col-sm mt-2' >
                        <Card className='card mb-4'
                            onClick={() => window.open(item.matchviewUrl)}>
                            {/* <h6 className='text-primary'>{item.competition}</h6> */}
                            <Card.Img src={item.thumbnail} />
                            <Card.Body >
                                <Card.Title>{item.title}</Card.Title>
                                <Card.Title>{item.date}</Card.Title> 
                            </Card.Body>
                        </Card>
                    </Col>

                ))
            }

        </Row>




    )
}

export default Post